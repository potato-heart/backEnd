//引入模型
require('./../models/pointsModel');
const mongoose = require('mongoose');

//获取所有的知识点
async function getAllPoints(req,res,next) {
    let points = await mongoose.model('pointsModel').find({});
    console.log(points);
    res.send({ points, code: 200 });
}
// getAllPoints()
module.exports = {
    getAllPoints
}