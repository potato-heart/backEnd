const expressJWT = require('express-jwt');
const jwtAuth = expressJWT({
    //密钥必须是跟生成token用的是一样的
    secret: 'user',
    algorithms: ['HS256'], // 设置 jwt 的算法
    // 设置为 false，表示如果不带 token 的请求，不进行验证
    // 设置为 true，表示请求带不带 token 都要验证，如果不带 token 的请求，直接验证失败
    credentiaalsRequired: false
}).unless({
    // 不需要进行验证的路由,即白名单
    path: ['/users/login', '/users/register']
})
module.exports = jwtAuth;