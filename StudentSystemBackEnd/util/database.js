//连接数据库
const mongoose = require('mongoose');//引入mongoose
const dbURI = 'mongodb://localhost:27017/examSystemData';  // 其中 test 为连接的数据库名称
mongoose.connect(dbURI, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.connection.on('connected', function() {  
	console.log('Mongoose 已连接到 ' + dbURI);
});