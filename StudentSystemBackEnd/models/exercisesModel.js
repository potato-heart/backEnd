
const mongoose = require('mongoose');
//引入知识点数据库
require('./../models/pointsModel')
const exercisesSchema = new mongoose.Schema({
    analysis:String,
    answer:Number,
    options:Array,
    pointId:{
        //需要找知识点
        type: mongoose.SchemaTypes.ObjectId,
        ref:'pointModel'
    },
    score:Number,
    topics:String

});
mongoose.model('exercisesModel', exercisesSchema, 'exercises');